import { Navigate } from "react-router-dom";

function ProtectedRoute({ children }) {

  return sessionStorage.getItem('token') !== null ? children : <Navigate to="/login" />;

}

export default ProtectedRoute;