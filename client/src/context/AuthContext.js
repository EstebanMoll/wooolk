import React, { useEffect, createContext } from 'react';
import jwt_decode from 'jwt-decode';
import { useNavigate } from 'react-router-dom';

export const AuthContext = createContext();

const AuthContextProvider = ({ children }) => {

  const navigate = useNavigate();

  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            token: action.token,
            id: action.id,
            name: action.name,
            email: action.email,
            city: action.city,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            token: action.token,
            id: action.id,
            name: action.name,
            email: action.email,
            city: action.city,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            token: null,
            id: null,
            name: null,
            email: null,
            city: null,
          };
        case 'SIGN_UP':
          return {
            ...prevState,
            token: action.token,
            id: action.id,
            name: action.name,
            email: action.email,
            city: action.city,
          }
      }
    },
    {
      token: null,
      id: null,
      name: null,
      email: null,
      city: null,
    }
  );

  useEffect(() => {

    const checkIfUserExist = async () => {
      let userToken;
      try {
        userToken = sessionStorage.getItem('token');
        if (userToken) {
          const resp = await fetch(
            process.env.REACT_APP_IPADRESSESERVER + `/users/me`,
            {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'token': userToken,
              }
            },
          );

          const respJSON = await resp.json();
          if (!resp.ok) {
            console.log('error');
            console.log(JSON.stringify(resp.body));
          }

          if (respJSON) {
            var decoded = jwt_decode(userToken);
            dispatch({
              type: 'RESTORE_TOKEN',
              token: userToken,
              id: decoded.user.id,
              name: respJSON.name,
              email: respJSON.email,
              city: respJSON.city,
            });
          }
        }
      }
      catch (e) {
        console.log(e);
      }
    };

    checkIfUserExist();
  }, []);


  const signIn = async (data, route) => {
    try {
      const resp = await fetch(
        process.env.REACT_APP_IPADRESSESERVER + `/users/login`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: data.email,
            password: data.password,
          }),
        }
      );
      const respJSON = await resp.json();
      if (!resp.ok) {
        console.log('error');
        console.log(JSON.stringify(resp.body));
      }

      if (respJSON.token) {
        sessionStorage.setItem('token', respJSON.token);
        var decoded = jwt_decode(respJSON.token);
        dispatch({
          type: 'SIGN_IN',
          token: respJSON.token,
          id: decoded.user.id,
          name: respJSON.name,
          email: respJSON.email,
          city: respJSON.city,
        });
        navigate(route);
      }

    } catch (error) {
      console.log('errorCatch', error);
    }
  };

  const signOut = async () => {
    sessionStorage.setItem('token', '');
    dispatch({ type: 'SIGN_OUT' });
  };

  const signUp = async (data, route) => {
    try {
      const resp = await fetch(
        process.env.REACT_APP_IPADRESSESERVER + `/users/signup`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            name: data.name,
            email: data.email,
            password: data.password,
            city: data.city,
          }),
        }
      );
      const respJSON = await resp.json();
      if (!resp.ok) {
        console.log('error : ' + resp);
        console.log(JSON.stringify(resp.body));
      }

      if (respJSON.token) {
        sessionStorage.setItem('token', respJSON.token);
        var decoded = jwt_decode(respJSON.token);
        dispatch({
          type: 'SIGN_UP',
          token: respJSON.token,
          id: decoded.user.id,
          name: respJSON.name,
          email: respJSON.email,
          city: respJSON.city,
        });
        navigate(route);
      }

    } catch (error) {
      console.log('errorCatch', error);
    }
  };

  return (
    <AuthContext.Provider
      value={{
        user: state,
        signIn: signIn,
        signOut: signOut,
        signUp: signUp,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContextProvider;
