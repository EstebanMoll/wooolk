import { useState, useContext } from 'react';
import logo from '../../Resources/Logo_Wooolk.png'
import { AuthContext } from '../../context/AuthContext';
 
function Login () { 

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { signIn } = useContext(AuthContext);

    const submit = () => {
      let data = {
          email: email,
          password: password
      }
      signIn(data, "/");
    }

  return (
    <div className="App">
      <img src={logo}/>
        <div>
          <div>
            <input type="text" placeholder="Email" onChange={data => setEmail(data.target.value)}/>
          </div>
          <div style={{ marginTop: 20 }}>
            <input type="password" placeholder="Mot de passe" onChange={data => setPassword(data.target.value)} />
          </div>
          <div style={{ marginTop: 20 }}>
            <button onClick={() => submit()}>Connexion</button>
          </div>
          <div style={{ marginTop: 50 }}>
            <p>Mot de passe oublié</p>
            <a href="/signup">Créer un compte</a>
          </div>
      </div>
    </div>
  );  
}

export default Login;