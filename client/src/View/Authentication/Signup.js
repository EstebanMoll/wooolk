import { useState, useContext } from 'react';
import logo from '../../Resources/Logo_Wooolk.png'
import { AuthContext } from '../../context/AuthContext';
import './Signup.css';
import { useNavigate } from 'react-router-dom';


function Signup() {

    const [name, setName] = useState();
    const [email, setEmail] = useState();
    const [city, setCity] = useState();
    const [password, setPassword] = useState();
    const [confirmPassword, setConfirmPassword] = useState();
    const [errorMsg, setErrorMsg] = useState();
    const [errorEmail, setErrorEmail] = useState();
    const { signUp } = useContext(AuthContext);

    const navigate = useNavigate();

    const redirectLogin = () => {
        navigate("/login");
    }

    let handleOnChange = (email) => {

        // don't remember from where i copied this code, but this works.
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (re.test(email)) {
            setErrorEmail();
            setEmail(email);
        }
        else {
            setErrorEmail("Email invalide");
        }

    }

    const submit = () => {
        if (name && email && city && password && confirmPassword) {
            if (password.length < 6) {
                setErrorMsg("Le mot de passe doit faire au moins 6 caractères");
            }
            else if (password !== confirmPassword) {
                setErrorMsg("Les mots de passes sont différents");
            }
            else {
                setErrorMsg();
                let data = {
                    name: name,
                    email: email,
                    password: password,
                    city: city
                }
                signUp(data, "/");
            }
        }
        else {
            setErrorMsg("Veuillez remplir tous les champs");
        }
    }

    return (
        <div className="Signup">
            <div>
                <img src={logo} />
                <button className="btn-right" onClick={() => redirectLogin()}>Retour au login</button>
            </div>
            <div className="divSignup">
                <input type="text" placeholder="Nom" onChange={data => setName(data.target.value)} />
            </div>
            <div className="divSignup">
                <input type="email" placeholder="Email" onChange={data => handleOnChange(data.target.value)} />
                {errorEmail && <p style={{ color: 'red' }}> {errorEmail} </p>}
            </div>
            <div className="divSignup">
                <input type="text" placeholder="Ville" onChange={data => setCity(data.target.value)} />
            </div>
            <div className="divSignup">
                <input type="password" placeholder="Mot de passe" onChange={data => setPassword(data.target.value)} />
            </div>
            <div className="divSignup">
                <input type="password" placeholder="Confirmation de mot de passe" onChange={data => setConfirmPassword(data.target.value)} />
            </div>
            {errorMsg && <div className="divSignup"> <p style={{ color: 'red' }}> {errorMsg} </p> </div>}
            <div className="divSignup">
                <button onClick={() => submit()}>Créer un compte</button>
            </div>
        </div>
    );
}

export default Signup;