import logo from '../../Resources/Logo_Wooolk.png'
import './home.css';
import Reiko from '../../Resources/Reiko.jpg';
import { useEffect, useState, useContext } from 'react';
import Carousel from 'react-elastic-carousel';
import { useNavigate } from 'react-router-dom';

function Home() {

  const [toutous, setToutous] = useState([]);

  const navigate = useNavigate();

  const handleLogout = () => {
    sessionStorage.clear();
    navigate('/login');
  }

  const handleProfile = () => {
    navigate('/Profile');
  }

  const breakPoints = [
    { width: 1, itemsToShow: 1 },
    { width: 550, itemsToShow: 2, itemsToScroll: 2 },
    { width: 700, itemsToShow: 3 },
    { width: 950, itemsToShow: 4, itemsToScroll: 2 },
    { width: 1450, itemsToShow: 5 },
    { width: 1750, itemsToShow: 6 },
  ];

  function getAge(birthDate) {
    var tmp = (new Date() - new Date(birthDate).getTime()) / 3.15576e+10;
    if (tmp < 1) {
      return Math.floor(tmp * 12) + ' mois';
    }
    else {
      return Math.floor(tmp) + ' ans';
    }
  }

  const getToutousByName = async (name) => {
    let userToken;
    try {
      userToken = sessionStorage.getItem('token');
      if (userToken) {
        const resp = await fetch(
          process.env.REACT_APP_IPADRESSESERVER + `/toutous/getToutousByName?name=${encodeURIComponent(name)}`,
          {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              'token': userToken,
            },
          },
        );

        const respJSON = await resp.json();
        if (!resp.ok) {
          console.log('error');
          console.log(JSON.stringify(resp.body));
        }

        if (respJSON) {

          var TabToutous = [];

          respJSON.forEach(element => {
            var age = getAge(element.birthdate);

            TabToutous.push({ _id: element._id, user_id: element.user_id, name: element.name, character1: element.character1, character2: element.character2, age: age });

          });

          setToutous(TabToutous);
        }
      }
    }
    catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {

    const getToutous = async () => {
      let userToken;
      try {
        userToken = sessionStorage.getItem('token');
        if (userToken) {
          const resp = await fetch(
            process.env.REACT_APP_IPADRESSESERVER + `/toutous/getAllToutous`,
            {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'token': userToken,
              }
            },
          );

          const respJSON = await resp.json();
          if (!resp.ok) {
            console.log('error');
            console.log(JSON.stringify(resp.body));
          }

          if (respJSON) {

            var TabToutous = [];

            respJSON.forEach(element => {
              var age = getAge(element.birthdate);

              TabToutous.push({ _id: element._id, user_id: element.user_id, name: element.name, character1: element.character1, character2: element.character2, age: age });

            });

            setToutous(TabToutous);
          }
        }
      }
      catch (e) {
        console.log(e);
      }
    };

    getToutous();
  }, []);

  return (
    <div>
      <div>
        <img src={logo} />
        <button className='btn-topRight' onClick={handleLogout}>Déconnexion</button>
        <button className='btn-topRight' onClick={handleProfile}>Mon profil</button>
      </div>
      <div style={{ display: "flex" }}>
        <div style={{ flex: "2" }}>
          <div style={{ alignitems: "center", textalign: "center" }}>
            <input className='search' placeholder="Recherche" onChange={data => getToutousByName(data.target.value)} />
          </div>
          <div style={{ marginTop: '50px' }}>
            <Carousel breakPoints={breakPoints}>
              {toutous.map(item =>
                <div key={item._id} style={{ backgroundColor: "white", borderRadius: "15%" }}>
                  <div>
                    <img src={Reiko} style={{ borderRadius: "15%" }} />
                  </div>
                  <div style={{ paddingLeft: "5%" }}>
                    <p style={{ fontWeight: "bold" }}>{item.name}, {item.age}</p>
                    <p>{item.character1}, {item.character2}</p>
                  </div>
                </div>)}
            </Carousel>
          </div>
        </div>
        <div style={{ flex: "1", height: "550px" }}>
          <div style={{ backgroundColor: "white", margin: "10px", height: "550px", borderRadius: "25px", borderColor: "black" }}>
            <h1 style={{ textAlign: "center" }}>Conversations</h1>
            <p style={{ textAlign: "center" }}>Bientot disponible</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;