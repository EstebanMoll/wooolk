import { useEffect, useState, useContext } from 'react';
import logo from '../../Resources/Logo_Wooolk.png'
import { AuthContext } from '../../context/AuthContext';
import { useNavigate } from 'react-router-dom';
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

function Toutou() {
    const { user } = useContext(AuthContext);

    const [name, setName] = useState('');
    const [birthdate, setBirthdate] = useState();
    const [character1, setCharacter1] = useState('Joueur');
    const [character2, setCharacter2] = useState('Joueur');
    const [images, setImages] = useState([]);
    const [imageURLs, setImageURLs] = useState([]);

    const navigate = useNavigate();

    const onImageChange = async (e) => {
        setImages([...e.target.files])
    }

    const handleReturnProfile = () => {
        navigate('/Profile');
    }

    const convertToBase64 = (file) => {
        return new Promise(resolve => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                resolve(reader.result);
            }
        })
    }

    const submit = async () => {
        if (name && birthdate && character1 && character2 && images[0]) {
            try {
                let userToken = sessionStorage.getItem('token');
                if (userToken) {
                    var convertedFile = await convertToBase64(images[0]);
                    const resp = await fetch(
                        process.env.REACT_APP_IPADRESSESERVER + `/toutous/addToutou`,
                        {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                                'token': userToken,
                            },
                            body: JSON.stringify({
                                name: name,
                                birthdate: birthdate,
                                character1: character1,
                                character2: character2,
                                image: convertedFile,
                                imageName: images[0].name
                            }),
                        }
                    );

                    const respJSON = await resp.json();

                    if (!resp.ok) {
                        console.log('error');
                        console.log(JSON.stringify(resp.body));
                    }

                    if (respJSON.msg === "Toutou ajoute") {
                        navigate("/Profile");
                    }
                }
            } catch (error) {
                console.log('errorCatch', error);
            }
        }
    }

    useEffect(() => {
        if (images.length < 1) return;
        const newImageUrls = [];
        images.forEach(image => newImageUrls.push(URL.createObjectURL(image)));
        setImageURLs(newImageUrls);
    }, [images]);

    return (
        <div>
            <div>
                <img src={logo} />

                <button className='btn-topRight' onClick={handleReturnProfile}>Retour à mon profil</button>
            </div>
            <h1 style={{ flex: "3", textAlign: "center" }} >Ajouter mon toutou</h1>
            <div style={{ display: "flex" }}>
                <div style={{ flex: "1", flexDirection: "column", display: "flex", justifyContent: "center", alignItems: "center" }}>
                    <div>
                        <input type="text" placeholder="Nom" onChange={data => setName(data.target.value)} />
                    </div>
                    <div>
                        <DatePicker placeholderText='Date de naissance' dateFormat="yyyy-MM-dd" selected={birthdate} onChange={(date) => setBirthdate(date)} />
                    </div>
                    <div>
                        <select placeholder='"Traits de caractère' onChange={data => setCharacter1(data.target.value)}>
                            <option value='Joueur'>Joueur</option>
                            <option value='Amical'>Amical</option>
                            <option value='Agressif'>Agressif</option>
                            <option value='Timide'>Timide</option>
                            <option value='Independant'>Independant</option>
                        </select>
                    </div>
                    <div>
                        <select placeholder='"Traits de caractère' onChange={data => setCharacter2(data.target.value)}>
                            <option value='Joueur'>Joueur</option>
                            <option value='Amical'>Amical</option>
                            <option value='Agressif'>Agressif</option>
                            <option value='Timide'>Timide</option>
                            <option value='Independant'>Independant</option>
                        </select>
                    </div>
                </div>
                <div style={{ flex: "1" }}>
                    <p style={{ textAlign: "center" }}>Ajouter une photo</p>
                    <input type="file" accept='image/*' onChange={onImageChange} />
                    {imageURLs.map(imageSrc => <img src={imageSrc} />)}
                </div>
            </div>
            <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                <button onClick={() => submit()}>Ajouter</button>
            </div>
        </div>
    );
}

export default Toutou;