import { useEffect, useState, useContext } from 'react';
import logo from '../../Resources/Logo_Wooolk.png';
import Reiko from '../../Resources/Reiko.jpg';
import { AuthContext } from '../../context/AuthContext';
import Carousel from 'react-elastic-carousel';
import { useNavigate } from 'react-router-dom';

function Profile() {
    const { user } = useContext(AuthContext);
    const [toutous, setToutous] = useState([]);

    const navigate = useNavigate();

    const handleReturnHome = () => {
        navigate('/');
    }

    const handleAddToutou = () => {
        navigate('/Profile/AddToutou');
    }

    const handleUpdateToutou = (id) => {
        navigate('/Profile/UpdateToutou', {
            state: {
                toutouId: id,
            }
        });
        //navigate("/Profile/UpdateToutou");
    }

    const breakPoints = [
        { width: 1, itemsToShow: 1 },
        { width: 550, itemsToShow: 2, itemsToScroll: 2 },
        { width: 700, itemsToShow: 3 },
        { width: 950, itemsToShow: 4, itemsToScroll: 2 },
        { width: 1450, itemsToShow: 5 },
        { width: 1750, itemsToShow: 6 },
    ];

    function getAge(birthDate) {
        var tmp = (new Date() - new Date(birthDate).getTime()) / 3.15576e+10;
        if (tmp < 1) {
            return Math.floor(tmp * 12) + ' mois';
        }
        else {
            return Math.floor(tmp) + ' ans';
        }
    }

    useEffect(() => {

        const getToutous = async () => {
            let userToken;
            try {
                userToken = sessionStorage.getItem('token');
                if (userToken) {
                    const resp = await fetch(
                        process.env.REACT_APP_IPADRESSESERVER + `/toutous/getToutous`,
                        {
                            method: 'GET',
                            headers: {
                                'Content-Type': 'application/json',
                                'token': userToken,
                            }
                        },
                    );

                    const respJSON = await resp.json();
                    if (!resp.ok) {
                        console.log('error');
                        console.log(JSON.stringify(resp.body));
                    }

                    if (respJSON) {

                        var TabToutous = [];

                        respJSON.forEach(element => {
                            var age = getAge(element.birthdate);

                            TabToutous.push({ _id: element._id, user_id: element.user_id, name: element.name, character1: element.character1, character2: element.character2, age: age });

                        });

                        setToutous(TabToutous);
                    }
                }
            }
            catch (e) {
                console.log(e);
            }
        };

        getToutous();
    }, []);



    return (
        <div>
            <div>
                <img src={logo} />
                <button className='btn-topRight' onClick={handleReturnHome}>Retour à la page d'accueil</button>
            </div>
            <div style={{ display: "flex" }}>
                <div style={{ flex: "1" }}>
                    <p>Nom : {user.name}</p>
                    <p>Adresse mail : {user.email}</p>
                    <p>Ville : {user.city}</p>
                    <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                        <button >Modifier mon mot de passe</button>
                    </div>
                </div>
                <div style={{ flex: "2" }}>
                    <h1 style={{ textAlign: "center", margin: "0" }}>Mes toutous</h1>
                    <div style={{ margin: "5%" }}>
                        <Carousel breakPoints={breakPoints}>
                            {toutous.map(item =>
                                <div style={{ textAlign: "center" }}>
                                    <div key={item._id} style={{ textAlign: "start", backgroundColor: "white", borderRadius: "15%" }}>
                                        <div>
                                            <img src={Reiko} style={{ borderRadius: "15%" }} />
                                        </div>
                                        <div style={{ paddingLeft: "5%" }}>
                                            <p style={{ fontWeight: "bold" }}>{item.name}, {item.age}</p>
                                            <p>{item.character1}, {item.character2}</p>
                                        </div>
                                    </div>
                                    <div style={{ display: "inline-block" }}>
                                        <button onClick={() => handleUpdateToutou(item._id)}>Modifier</button>
                                    </div>
                                </div>)}
                        </Carousel>
                    </div>
                    <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                        <button onClick={handleAddToutou}>Ajouter un toutou</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Profile;