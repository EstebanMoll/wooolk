import { useEffect, useState, useContext } from 'react';
import logo from '../../Resources/Logo_Wooolk.png'
import { AuthContext } from '../../context/AuthContext';
import { useLocation, useNavigate } from 'react-router-dom';
import Reiko from '../../Resources/Reiko.jpg';
import Popup from 'reactjs-popup';


function UpdateToutou() {
    const { user } = useContext(AuthContext);

    const [character1, setCharacter1] = useState('');
    const [character2, setCharacter2] = useState('');
    const [images, setImages] = useState([]);
    const [imageURLs, setImageURLs] = useState([]);
    const [toutous, setToutous] = useState([]);

    const navigate = useNavigate();
    const location = useLocation();

    const onImageChange = async (e) => {
        setImages([...e.target.files])
    }

    const handleReturnProfile = () => {
        navigate('/Profile');
    }

    const convertToBase64 = (file) => {
        return new Promise(resolve => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                resolve(reader.result);
            }
        })
    }

    const update = async () => {
        if (character1 && character2) {
            try {
                let userToken = sessionStorage.getItem('token');
                if (userToken) {
                    const resp = await fetch(
                        process.env.REACT_APP_IPADRESSESERVER + `/toutous/updateMyToutou?_id=${encodeURIComponent(location.state.toutouId)}`,
                        {
                            method: 'PATCH',
                            headers: {
                                'Content-Type': 'application/json',
                                'token': userToken,
                            },
                            body: JSON.stringify({
                                character1: character1,
                                character2: character2,
                            }),
                        }
                    );

                    const respJSON = await resp.json();

                    if (!resp.ok) {
                        console.log('error');
                        console.log(JSON.stringify(resp.body));
                    }

                    if (respJSON.acknowledged === true) {
                        navigate("/Profile");
                    }
                }
            } catch (error) {
                console.log('errorCatch', error);
            }
        }
    }

    const deleteMyToutou = async () => {
        try {
            let userToken = sessionStorage.getItem('token');
            if (userToken) {
                const resp = await fetch(
                    process.env.REACT_APP_IPADRESSESERVER + `/toutous/deleteMyToutou?_id=${encodeURIComponent(location.state.toutouId)}`,
                    {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                            'token': userToken,
                        },
                    }
                );

                const respJSON = await resp.json();

                if (!resp.ok) {
                    console.log('error');
                    console.log(JSON.stringify(resp.body));
                }

                if (respJSON.deletedCount == 1) {
                    navigate("/Profile");
                }
            }
        } catch (error) {
            console.log('errorCatch', error);
        }
    }

    useEffect(() => {
        const getMyToutou = async () => {
            let userToken;
            try {
                userToken = sessionStorage.getItem('token');
                if (userToken) {
                    const resp = await fetch(
                        process.env.REACT_APP_IPADRESSESERVER + `/toutous/getMyToutou?_id=${location.state.toutouId}`,
                        {
                            method: 'GET',
                            headers: {
                                'Content-Type': 'application/json',
                                'token': userToken,
                            }
                        },
                    );

                    const respJSON = await resp.json();
                    if (!resp.ok) {
                        console.log('error');
                        console.log(JSON.stringify(resp.body));
                    }

                    if (respJSON) {
                        setCharacter1(respJSON.character1);
                        setCharacter2(respJSON.character2);
                    }
                }
            }
            catch (e) {
                console.log(e);
            }
        };

        getMyToutou();
        if (images.length < 1) return;
        const newImageUrls = [];
        images.forEach(image => newImageUrls.push(URL.createObjectURL(image)));
        setImageURLs(newImageUrls);

    }, [images, location.state.toutouId]);

    return (
        <div>
            <div>
                <img src={logo} />
                <button className='btn-topRight' onClick={handleReturnProfile}>Retour à mon profil</button>
            </div>
            <div style={{ display: "flex", flexDirection: "column" }}>
                <h1 style={{ flex: "1", textAlign: "center" }} >Modifier mon toutou</h1>
                <div style={{ flex: "10", display: "flex" }}>
                    <div style={{ flex: "1", flexDirection: "column", display: "flex", justifyContent: "center", alignItems: "center" }}>
                        <div>
                            <select value={character1} onChange={data => setCharacter1(data.target.value)}>
                                <option value='Joueur'>Joueur</option>
                                <option value='Amical'>Amical</option>
                                <option value='Agressif'>Agressif</option>
                                <option value='Timide'>Timide</option>
                                <option value='Independant'>Independant</option>
                            </select>
                        </div>
                        <div>
                            <select value={character2} onChange={data => setCharacter2(data.target.value)}>
                                <option value='Joueur'>Joueur</option>
                                <option value='Amical'>Amical</option>
                                <option value='Agressif'>Agressif</option>
                                <option value='Timide'>Timide</option>
                                <option value='Independant'>Independant</option>
                            </select>
                        </div>
                    </div>
                    <div style={{ flex: "1" }}>
                        <div>
                            <p style={{ textAlign: "center" }}>Modifier la photo</p>
                            <img src={Reiko} style={{ borderRadius: "15%", }} />
                        </div>
                        <input type="file" accept='image/*' onChange={onImageChange} />
                        {imageURLs.map(imageSrc => <img src={imageSrc} />)}
                    </div>
                </div>
                <div style={{ display: "flex", justifyContent: "center" }}>
                    <div style={{ margin: "2%" }}>
                        <button onClick={() => update()}>Modifier</button>
                    </div>
                    <div style={{ margin: "2%" }}>
                        <button style={{ backgroundColor: "red", borderColor: "red" }} onClick={() => deleteMyToutou()}>Supprimer</button>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default UpdateToutou;