import React, { useContext } from "react";
import './App.css';
import logo from './Resources/Logo_Wooolk.png'
import Home from './View/Home';
import Profile from './View/Profile/Profile';
import Toutou from './View/Profile/Toutou';
import Login from './View/Authentication/Login';
import Signup from "./View/Authentication/Signup";
import UpdateToutou from './View/Profile/UpdateToutou';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ProtectedRoute from "./components/ProtectedRoute";

function App() {

  // const { user } = useContext(AuthContext);

  return (
    // <BrowserRouter>
    <Routes>
      <Route exact path="/" element={
        <ProtectedRoute>
          <Home />
        </ProtectedRoute>
      }
      />
      <Route exact path="/Profile" element={
        <ProtectedRoute>
          <Profile />
        </ProtectedRoute>
      }
      />
      <Route exact path="/Profile/AddToutou" element={
        <ProtectedRoute>
          <Toutou />
        </ProtectedRoute>
      }
      />
      <Route exact path="/Profile/UpdateToutou" element={
        <ProtectedRoute>
          <UpdateToutou />
        </ProtectedRoute>
      }
      />
      <Route exact path="/login" element={<Login />} />
      <Route exact path="/signup" element={<Signup />} />
    </Routes>
    //</BrowserRouter> 
  );
}

export default App;
