const fs = require('fs');

const AWS = require('aws-sdk');
const BUCKET_NAME = process.env.IMAGES_BUCKET;
AWS.config.update({ region: process.env.REGION });
const options = {
    apiVersion: '2006-03-01',
    params: {
        Bucket: BUCKET_NAME
    },
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
}
const s3 = new AWS.S3(options);

/**
 * @description Uploads an image to S3
 * @param imageName Image name
 * @param base64Image Image body converted to base 64
 * @param type Image type
 * @return string S3 image URL or error accordingly
 */
async function upload(imageName, image, type) {
    // var imageAsStream;
    // fs.readFile(image, (err, data) => {
    //     if (err) throw err;
    //     image = data;
    // });
    // console.log("image : " + imageAsStream)
    const params = {
        Bucket: `${BUCKET_NAME}`,
        Key: imageName,
        Body: Buffer.from(image.replace(/^data:image\/\w+;base64,/, ""), 'base64')
    };

    let data;

    try {
        //data = await promiseUpload(params);
        data = await s3.upload(params).promise();
        console.log('the upload', data);
    } catch (err) {
        console.error(err);

        return "";
    }

    return data.Location;
}

/**
 * @description Promise an upload to S3
 * @param params S3 bucket params
 * @return data/err S3 response object
 */
function promiseUpload(params) {
    return new Promise(function (resolve, reject) {
        s3.upload(params, function (err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

module.exports = { upload };