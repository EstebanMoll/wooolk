const path = require('path');
const express = require("express");
const bcrypt = require('bcrypt');
const session = require('express-session');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const users = require('./routes/users');
const toutous = require('./routes/toutous');
require('dotenv').config();
const bodyParser = require("body-parser");
const InitiateMongoServer = require("./config/db");
const cors = require('cors');

const PORT = process.env.PORT || 3001;

InitiateMongoServer();

const app = express();

app.use(bodyParser.json({limit: '50mb'}));

app.use(cors({
  origin: '*'
}));

// Handle GET requests to /api route
app.get("/api", (req, res) => {
  res.json({ message: "Hello from server!" });
});

/**
 * Router Middleware
 * Router - /user/*
 * Method - *
 */
 app.use("/users", users);

 /**
 * Router Middleware
 * Router - /toutou/*
 * Method - *
 */
  app.use("/toutous", toutous);

// All other GET requests not handled before will return our React app
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../client/build', 'index.html'));
});

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});