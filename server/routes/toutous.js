const express = require("express");
const { check, validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const router = express.Router();
const auth = require("../middleware/auth");
const imagesService = require("../services/imagesService");

const { Toutou } = require("../models/toutou");
const { decode } = require("jsonwebtoken");

// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
var fs = require('fs');
var path = require('path');
const res = require("express/lib/response");


// Set the region 
AWS.config.update({ region: 'eu-west-3' });

// Create S3 service object
s3 = new AWS.S3({ apiVersion: '2006-03-01' });

// Create the parameters for calling listObjects
var bucketParams = {
    Bucket: 'wooolkbucket',
};

// call S3 to retrieve upload file to specified bucket
var uploadParams = { Bucket: 'wooolkbucket', Key: '', Body: '' };



/**
 * @method - POST
 * @param - /addToutou
 * @description - Toutou Add
 */

router.post(
    "/addToutou",
    auth,
    [
        check("name", "Please Enter a Valid Name")
            .not()
            .isEmpty(),
        check("birthdate", "Please enter a valid birthdate").isISO8601(),
        check("character1", "Please enter a valid caractere").not().isEmpty(),
        check("character2", "Please enter a valid caractere").not().isEmpty(),
        check("character2", "Please select 2 differente caractere").not().equals("character1"),
        // check("image", "Please enter a valid base64").not().isEmpty(),
        check("imageName", "Please enter a valid image name").not().isEmpty()
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array()
            });
        }

        const {
            name,
            birthdate,
            character1,
            character2,
            image,
            imageName
        } = req.body;
        try {
            const decoded = jwt.verify(req.header("token"), process.env.RANDOM_STRING);
            let user_id = decoded.user.id;

            let toutou = await Toutou.findOne({ user_id: user_id, name: name, birthdate: birthdate });

            if (toutou) {
                return res.status(400).json({
                    msg: "Toutou Already Exists"
                });
            }

            //var response = await imagesService.upload(imageName, image);

            //if (response) {
            toutou = new Toutou({
                user_id,
                name,
                birthdate,
                character1,
                character2//,
                // response
            });

            let saveToutou = await toutou.save();

            if (saveToutou) {
                res.status(200).json({
                    msg: "Toutou ajoute"
                });
            }
            //}

        } catch (err) {
            console.log(err.message);
            res.status(500).send("Error in Saving");
        }
    }
);

router.get("/getToutous", auth, async (req, res) => {
    try {
        const decoded = jwt.verify(req.header("token"), process.env.RANDOM_STRING);
        let user_id = decoded.user.id;
        const toutous = await Toutou.find({ "user_id": user_id });
        res.json(toutous);
    } catch (e) {
        res.send({ message: "Error in Fetching toutous" });
    }
});

router.get("/getAllToutous", auth, async (req, res) => {
    try {
        const toutous = await Toutou.find();
        res.json(toutous);
    } catch (e) {
        res.send({ message: "Error in Fetching all toutous" });
    }
})

router.get("/getToutousByName", auth, async (req, res) => {
    try {
        const toutous = await Toutou.find({ name: { $regex: '.*' + req.query.name + '.*', $options: 'i' } });
        res.json(toutous);
    }
    catch (e) {
        res.send({ message: "Error in Fetching research toutous" });
    }
})

router.patch("/updateMyToutou", auth, async (req, res) => {
    try {
        var ret = await Toutou.updateOne({ _id: req.query._id }, { $set: { character1: req.body.character1, character2: req.body.character2 } });
        res.json(ret);
    }
    catch (e) {
        res.send({ message: "Error in Updating my toutou" });
    }
})

router.get("/getMyToutou", auth, async (req, res) => {
    try {
        const toutou = await Toutou.findById(req.query._id);
        res.json(toutou);
    }
    catch (e) {
        res.send({ message: "Error" });
    }
})

router.delete("/deleteMyToutou", auth, async (req, res) => {
    try {
        const toutouDelete = await Toutou.deleteOne({ _id: req.query._id });
        res.json(toutouDelete);
    }
    catch (e) {
        res.send({ message: "Error in Deleting my toutou" });
    }
})


module.exports = router;