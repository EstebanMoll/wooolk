const mongoose = require('mongoose');

const Toutou = mongoose.model('Toutou', new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    name: {
        type: String,
        required: true,
        minlength: 1,
        maxlength: 50
    },
    birthdate: {
        type: Date,
        required: true
    },
    character1: {
        type: String,
        required: true,
        enum: ['Joueur', 'Amical', 'Agressif', 'Timide', 'Independant'],
        default: 'Amical'
    },
    character2: {
        type: String,
        required: true,
        enum: ['Joueur', 'Amical', 'Agressif', 'Timide', 'Independant'],
        default: 'Joueur',
    }
    // },
    // image: {
    //     type: String,
    //     required: true
    // }
}));

module.exports.Toutou = Toutou;